var rootURL = "../users/";

function openMeasurements(email) {
    window.open(rootURL + email + "/measurements");
}

function exportToPdf(email) {
    window.location = (rootURL + email + "/pdf");
}
function exportToExcel(email) {
    window.location = (rootURL + email + "/excel");
}
