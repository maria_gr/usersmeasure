<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>Users list</title>
    <link rel="stylesheet" href="/css/styles.css"/>
</head>
<body>
<h1>Users</h1>

<ul>
    <c:forEach var="user" items="${usersList}">
        <li><span>${user.name}</span>
            <span>${user.email}</span>
            <button type="button" onclick="openMeasurements('${user.email}');">
                Measurements
            </button>
            <button type="button" onclick="exportToPdf('${user.email}');">
                PDF
            </button>
            <button type="button" onclick="exportToExcel('${user.email}');">
                Excel
            </button>
        </li>
    </c:forEach>
</ul>

<script src="/js/functions.js"></script>

</body>
</html>