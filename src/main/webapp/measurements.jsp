<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>User's measurements</title>
    <link rel="stylesheet" href="/css/styles.css"/>
</head>
<body>

<h1>${currentUser.name}'s measurements</h1>

<ul>
    <c:forEach var="measure" items="${measurementsList}">
        <li><span>${measure.name}</span>
            <span>${measure.value}</span>
        </li>
    </c:forEach>
</ul>

</body>
</html>