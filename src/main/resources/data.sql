insert into User (id, name, email)
values(1001,'User1', 'user1@mail.com');

insert into User (id, name, email)
values(1002,'User2', 'user2@mail.com');

insert into Measurement (id, name, value)
values(2001,'Measurement1', '125');

insert into Measurement (id, name, value)
values(2002,'Measurement2', '22');

insert into Measurement (id, name, value)
values(2003,'Measurement3', '33');

insert into USER_MEASUREMENTS
values(1001, 2001);

insert into USER_MEASUREMENTS
values(1001, 2002);

insert into USER_MEASUREMENTS
values(1002, 2003);