package usersmeasure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import usersmeasure.model.User;
import usersmeasure.report.ReportService;
import usersmeasure.service.UserService;

import java.io.ByteArrayOutputStream;
import java.util.Optional;

@Controller
public class UserController {
    private final UserService userService;

    private final ReportService reportService;

    @Autowired
    public UserController(UserService userService, ReportService reportService) {
        this.userService = userService;
        this.reportService = reportService;
    }

    @RequestMapping(value = "/users/{email}/measurements", method = RequestMethod.GET)
    public ModelAndView getUsersMeasurementsGet(@PathVariable String email) {
        ModelAndView model = new ModelAndView();
        model.addObject("currentUser", userService.getUserByEmail(email)
                .orElseThrow(() -> new IllegalArgumentException("User not found")));
        model.addObject("measurementsList", userService.getMeasurementsByUser(email));
        model.setViewName("measurements");
        return model;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView getUsers() {
        ModelAndView model = new ModelAndView("users");
        model.addObject("usersList", userService.getUsers());
        return model;
    }

    @RequestMapping(value = "/users/{email}/pdf", method = RequestMethod.GET, headers = "Accept=*/*")
    public ResponseEntity<byte[]> exportToPdf(@PathVariable String email) {
        Optional<User> optionalUser = userService.getUserByEmail(email);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.ok().build();
        }

        ByteArrayOutputStream outputStream = reportService.getUserMeasurementsAsStream(optionalUser.get(), "PDF");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "user_measurements.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/users/{email}/excel", method = RequestMethod.GET, headers = "Accept=*/*")
    public ResponseEntity<byte[]> exportToExcel(@PathVariable String email) {
        Optional<User> optionalUser = userService.getUserByEmail(email);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.ok().build();
        }

        ByteArrayOutputStream outputStream = reportService.getUserMeasurementsAsStream(optionalUser.get(), "XLS");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/xls"));
        String filename = "user_measurements.xls";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
        return response;
    }
}
