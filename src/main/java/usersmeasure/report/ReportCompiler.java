package usersmeasure.report;

import net.sf.jasperreports.engine.JRException;

import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;

public interface ReportCompiler {
    void compileReport(String filename, Collection<?> items, Map<String, Object> params,
                         String format, OutputStream output) throws JRException;
}
