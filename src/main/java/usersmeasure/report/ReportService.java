package usersmeasure.report;

import usersmeasure.model.User;

import java.io.ByteArrayOutputStream;

public interface ReportService {
    ByteArrayOutputStream getUserMeasurementsAsStream(User user, String format);
}
