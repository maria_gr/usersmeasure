package usersmeasure.report;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import usersmeasure.model.User;
import usersmeasure.report.model.UserMeasurementsModel;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportServiceImpl implements ReportService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ReportCompiler reportCompiler;

    @Autowired
    public ReportServiceImpl(ReportCompiler reportCompiler) {
        this.reportCompiler = reportCompiler;
    }

    @Override
    public ByteArrayOutputStream getUserMeasurementsAsStream(User user, String format) {
        List<UserMeasurementsModel> items = new ArrayList<>();
        user.getMeasurements().forEach(m -> {
            UserMeasurementsModel model = new UserMeasurementsModel();
            model.setMeasureName(m.getName());
            model.setMeasureValue(m.getValue());
            items.add(model);
        });

        Map<String, Object> params = new HashMap<>();
        params.put("userName", user.getName());
        params.put("userEmail", user.getEmail());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            reportCompiler.compileReport("jasperreports/user_measurements.jrxml", items, params, format, outputStream);
            writer.flush();
        } catch (JRException | IOException e) {
            logger.error("Report compilation error", e);
        }
        return outputStream;
    }
}
