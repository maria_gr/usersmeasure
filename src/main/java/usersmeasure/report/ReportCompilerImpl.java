package usersmeasure.report;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;

@Component
public class ReportCompilerImpl implements ReportCompiler {
    @Override
    public void compileReport(String filename, Collection<?> items, Map<String, Object> params,
                                String format, OutputStream output) throws JRException {
        InputStream reportStream = getClass().getResourceAsStream("/" + filename);
        JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);

        JRDataSource beanColDataSource = items == null ? new JREmptyDataSource() : new JRBeanCollectionDataSource(items);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanColDataSource);

        JRAbstractExporter exporter;

        switch (format) {
            case "PDF":
                exporter = new JRPdfExporter();
                break;
            case "XLS":
                exporter = new JRXlsExporter();
                break;
            default:
                throw new IllegalArgumentException("Wrong report type");
        }
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        exporter.exportReport();
    }
}
