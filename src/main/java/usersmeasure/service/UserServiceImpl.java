package usersmeasure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import usersmeasure.jpa.UserRepository;
import usersmeasure.model.Measurement;
import usersmeasure.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return Optional.ofNullable(repository.getUserByEmail(email));
    }

    @Override
    public List<Measurement> getMeasurementsByUser(String email) {
        if (email == null || email.isEmpty()) {
            return new ArrayList<>();
        }

        User user = repository.getUserByEmail(email);
        if (user == null) {
            return new ArrayList<>();
        }

        return user.getMeasurements();
    }
}
