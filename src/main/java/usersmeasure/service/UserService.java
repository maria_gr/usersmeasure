package usersmeasure.service;

import usersmeasure.model.Measurement;
import usersmeasure.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getUsers();
    Optional<User> getUserByEmail(String email);
    List<Measurement> getMeasurementsByUser(String email);
}
